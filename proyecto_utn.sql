-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 10-12-2013 a las 00:41:37
-- Versión del servidor: 5.5.27
-- Versión de PHP: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `proyecto_utn`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `aulas`
--

CREATE TABLE IF NOT EXISTS `aulas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_grupo` int(11) NOT NULL,
  `nombre_aula` varchar(50) NOT NULL,
  `ubicacion` varchar(250) NOT NULL,
  `codigo_aula` varchar(50) NOT NULL,
  `finalizacion` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `aulas`
--

INSERT INTO `aulas` (`id`, `id_grupo`, `nombre_aula`, `ubicacion`, `codigo_aula`, `finalizacion`) VALUES
(1, 1, 'A2', ' Lourdes frente al jardín de niños', '01', '2013-12-17 00:00:00'),
(2, 7, 'E7', 'Sede UTN en Lourdes frente al jardín de niños ', 'elour02', '2013-12-05 14:22:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `carreras`
--

CREATE TABLE IF NOT EXISTS `carreras` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_curso` int(11) NOT NULL,
  `nombre_carrera` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `codigo_carrera` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `carreras`
--

INSERT INTO `carreras` (`id`, `id_curso`, `nombre_carrera`, `codigo_carrera`) VALUES
(1, 2, 'Ingeniería del Software', 'ISW'),
(2, 2, 'Contabilidad y Finanzas', 'COFI');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE IF NOT EXISTS `cursos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `codigo_curso` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `nombre_curso` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  `creado_en` datetime NOT NULL,
  `actualizado_en` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`id`, `codigo_curso`, `nombre_curso`, `descripcion`, `creado_en`, `actualizado_en`) VALUES
(1, 'isw-512', 'Matematicas discretas', 'Curso teorico', '2013-12-05 02:04:00', '2013-12-12 03:11:00'),
(2, 'ISW-712', 'Programación ambiente web', 'Programación del frond end', '2013-12-05 14:22:00', '2013-12-02 14:22:00'),
(3, 'ISW-321', 'Redes computacionales', 'Curso práctico en laboratorio', '2013-12-08 01:03:04', '2013-12-09 06:06:05'),
(4, 'ISW-112', 'Ingeniería de Software', 'Curso de investigación', '2013-12-08 00:00:00', '2013-12-09 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grupos`
--

CREATE TABLE IF NOT EXISTS `grupos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `curso_id` int(11) NOT NULL,
  `profesor_id` int(11) NOT NULL,
  `numero_grupo` int(11) NOT NULL,
  `cuatrimestre` enum('I','II','III') CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `creado_en` datetime NOT NULL,
  `actualizado_en` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `grupos`
--

INSERT INTO `grupos` (`id`, `curso_id`, `profesor_id`, `numero_grupo`, `cuatrimestre`, `creado_en`, `actualizado_en`) VALUES
(1, 1, 5, 3, 'I', '2013-05-05 00:00:00', '2013-05-05 00:00:00'),
(2, 2, 2, 2, 'II', '2013-12-05 14:22:00', '2013-12-05 14:22:00'),
(3, 3, 3, 3, 'III', '2013-12-05 02:05:00', '2013-12-05 06:03:00'),
(4, 4, 2, 12, 'III', '2013-12-08 00:00:00', '2013-12-09 00:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `horarios`
--

CREATE TABLE IF NOT EXISTS `horarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_grupo` int(11) NOT NULL,
  `dia` varchar(100) COLLATE utf8_spanish_ci NOT NULL,
  `hora` varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  `meridiano` enum('am','pm') COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE IF NOT EXISTS `personas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cedula` varchar(50) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `categoria` enum('Estudiante','Profesor','Administrativo','s') CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `contrasenia` varchar(50) NOT NULL,
  `estado` enum('Activo','Inactivo') NOT NULL,
  `creado_en` datetime NOT NULL,
  `actualizado_en` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`id`, `cedula`, `nombre`, `apellidos`, `correo`, `categoria`, `usuario`, `contrasenia`, `estado`, `creado_en`, `actualizado_en`) VALUES
(1, '10330223', 'Luis Carlos', 'Loria Mezén', 'luis@correo.com', 'Estudiante', 'lclm', 'lclm123', 'Inactivo', '2013-12-04 04:08:08', '2013-12-04 05:05:04'),
(2, '205750211', 'Nancy Vanessa', 'Porras Navarro', 'nanaporras@gmail.cm', 'Profesor', 'nana', 'nana123', 'Activo', '2013-12-04 17:10:09', '2013-12-04 18:15:08'),
(3, '345', 'Patricia', 'Rocha', 'patri@utn.com', 'Administrativo', 'pp', 'pp', 'Activo', '2013-12-05 02:22:00', '2013-12-05 03:33:00'),
(4, '123555555', 'Miguel Angel', 'Rodríguez Mena', 'miguel@yahoo.com', 'Estudiante', 'xx', 'xx', 'Activo', '2013-08-08 12:11:00', '2013-08-08 12:11:00'),
(9, '1001', 'super', 'usuario', 'superusuario@gmail.com', 's', 'su', '1', 'Activo', '2013-12-06 04:07:00', '2013-12-06 08:09:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `registros`
--

CREATE TABLE IF NOT EXISTS `registros` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_estudiante` int(11) NOT NULL,
  `id_grupo` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `registros`
--

INSERT INTO `registros` (`id`, `id_estudiante`, `id_grupo`) VALUES
(1, 1, 1),
(2, 2, 2),
(3, 3, 3),
(4, 4, 1);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
