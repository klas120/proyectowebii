<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Controlador extends CI_Controller {
	
	//VARIABLE DE SECION
	private $session_id;
	
	function __construct(){
		parent::__construct();		
		
		$this->load->helper('form');//FORMULARIOS
		$this->load->library('table');//TABLAS		
		$this->load->model('modelo');		
		$this->load->helper('date');//FECHAS		
		//$this->load->helper('array');		
				
		//VARIABLE QUE RECIBE EL VALOR DE LOGIN O LOGOUT
		$this->session_id = $this->session->userdata('login');
		
	}
	//INDEX
	function index()
	{			
		//INICIO DE SESION DE LOS USUARIOS
		// IF DEL SUPER USUARIO 	
		if(($this->session->userdata('usuario') == 'su'))
        {	
			/*$datoPersona['variable']=$this->session->userdata('usuario');
            // $this->load->view('persona_v', compact('variable')); 
			 
			 $this->db->select();
         	 $datoPersona['records'] = $this->db->get('personas');			 									   		 
		     $this->load->view('persona_v', $datoPersona); 	*/	 
			 
			 
			 $sql = "SELECT * FROM personas WHERE categoria = 'Estudiante'";
				
				$datoMatricula['variable']=$this->session->userdata('usuario');					    	    
			    $datoMatricula['records'] = $this->db->query($sql);
			           	   
			   $this->load->view('matricula_v',$datoMatricula);			 
			 
			 
			 
			  /*$datoCarrera['variable']=$this->session->userdata('usuario');
			  $this->db->select();
         	  $datoCarrera['records'] = $this->db->get('carreras');
			  $this->load->view('principal',$datoCarrera); */
			 //#############################################
			 //  AQUI SOLO SE LLAMA A PRINCIPAL
			  //#############################################
			 
			 
		//ELSE DEL ESTUDIANTE	         
        }else if(($this->session->userdata('estado') == 'Activo') && 
		         ($this->session->userdata('categoria') == 'Estudiante')){				
			
			$idEstu=$this->session->userdata('id');
			$tablas['variable']=$this->session->userdata('usuario');			
		    $tablas['tpersona']= $this->modelo->tablaPersona($idEstu);
			
		    $sql2="SELECT cursos.codigo_curso, cursos.nombre_curso, cursos.descripcion 
			           FROM cursos WHERE cursos.id IN
			          (SELECT grupos.curso_id FROM grupos WHERE grupos.id IN
					  (SELECT id_grupo FROM registros WHERE id_estudiante = '+$idEstu+'))";
			
			$sql="SELECT numero_grupo, cuatrimestre FROM grupos WHERE id IN
			    (SELECT id_grupo FROM registros WHERE id_estudiante = '+$idEstu+')";
			
			$tablas['tcurso']= $this->db->query($sql2);		
			$tablas['tgrupo']= $this->db->query($sql);	
			//$tablas['tregistro']= $this->db->query($sql2);;
				
			$plantilla = array ( 'table_open'  => '<table  border="1" cellpadding="2" 
			                     cellspacing="1" class="btn btn-primary btn-lg">' );
            $this->table->set_template($plantilla);				
			$this->load->view('select_persona',$tablas);
           
        }
		// ELSE DE LOS PROFESORES
		else if(($this->session->userdata('estado') == 'Activo') && 
		         ($this->session->userdata('categoria') == 'Profesor')){
						
			$tablas['variable']=$this->session->userdata('usuario');
			
		    $tablas['tpersona']= $this->modelo->tablaPersona($this->session->userdata('id'));
		    //$tablas['tcurso']= $this->modelo->tablaProfesor_Grupo($this->session->userdata('id'));
			$miId = $this->session->userdata('id');
			$sql = "SELECT grupos.cuatrimestre, grupos.numero_grupo 
			        FROM grupos
			        WHERE grupos.profesor_id IN (SELECT curso_id FROM grupos WHERE grupos.profesor_id = '+$miId+')";
					
			$sql2 = "SELECT codigo_curso, nombre_curso, descripcion
			        FROM cursos
			        WHERE id in (select curso_id from grupos where profesor_id = '+$miId+')";
					
		     		 
			$tablas['tgrupo'] = $this->db->query($sql2);
			$tablas['tcurso']=	$this->db->query($sql);
			
			
			//CONFIGURACION DE LA TABLA	
			$plantilla = array ( 'table_open'  => '<table  border="1" cellpadding="2" 
			                     cellspacing="1" class="btn btn-primary btn-lg">' );
            $this->table->set_template($plantilla);
				
			$this->load->view('select_persona',$tablas);
           
        }
		// ELSE DE LOS ADMINISTRATIVOS
		else if(($this->session->userdata('estado') == 'Activo') && 
		         ($this->session->userdata('categoria') == 'Administrativo')){
						
			$tablas['variable']=$this->session->userdata('usuario');
			
		    $tablas['tpersona']= $this->modelo->tablaPersona($this->session->userdata('id'));		    
			$tablas['tgrupo'] = $this->db->query("SELECT cuatrimestre FROM grupos where id = 10000000");
			$tablas['tcurso']=	$this->db->query("SELECT descripcion FROM cursos where id = 10000000");
				
			$plantilla = array ( 'table_open'  => '<table  border="1" cellpadding="2" 
			                     cellspacing="1" class="btn btn-primary btn-lg">' );
            $this->table->set_template($plantilla);
				
			$this->load->view('select_persona',$tablas);
           
        }
		//USUARIO INACTIVO
		else if($this->session->userdata('estado')=='Inactivo'){
			$this->session->set_flashdata('ControllerMessage', 'Usuario existente, pero no ha sido activado!.');
                	redirect(base_url().'controlador/login',  301);
		}
		else{
			redirect(base_url().'controlador/login',  301);
		}
		 
	}
	//_______________________FIN INDEX _____________________________________________
	
			//LLAMA A REGISTROS PARA MOSTRAR EL MENSAJE DE EL USUARIO YA EXISTE!!!
			function llamarRegistro(){
				$this->load->view('registro');
			}
			// LLAMA A LOGIN PARA MOSTRAR EL MENSAJE DE REGISTRO REALIZADO!!!
			function llamarlogin(){
				$this->load->view('login');
			}	
			//______________________________________________________________
			
					
			function llamarCursos(){
			   $datoCurso['variable']=$this->session->userdata('usuario');
			   $this->db->select();
         	   $datoCurso['records'] = $this->db->get('cursos');
			   $this->load->view('curso_v',$datoCurso);
			}
			function llamarGrupos(){
			   $datoGrupo['variable']=$this->session->userdata('usuario');
			   $this->db->select();
         	   $datoGrupo['records'] = $this->db->get('grupos');
			   $this->load->view('grupo_v',$datoGrupo);
			}
			function llamarPersona(){
				$datoPersona['variable']=$this->session->userdata('usuario');
			   $this->db->select();
         	   $datoPersona['records'] = $this->db->get('personas');
			   //redirect(base_url().'controlador/persona_v/'+$datoPersona+'',  301);			   
			   $this->load->view('persona_v',$datoPersona);
			}
			
			//MATRICULAR NO RECIBE TABLAS #######
			function llamarMatricular(){			
				
				$sql = "SELECT * FROM personas WHERE categoria = 'Estudiante'";
				
				$datoMatricula['variable']=$this->session->userdata('usuario');					    	    
			    $datoMatricula['records'] = $this->db->query($sql);
			           	   
			   $this->load->view('matricula_v',$datoMatricula);
			}
			
			function llamarCarrera(){
				$datoCarrera['variable']=$this->session->userdata('usuario');
			   $this->db->select();
         	   $datoCarrera['records'] = $this->db->get('carreras');
			   $this->load->view('carrera_v',$datoCarrera);
			}
			function llamarPrincipal(){
			   $datoCarrera['variable']=$this->session->userdata('usuario');		   
			   $this->load->view('principal',$datoCarrera);
			}
			function llamarAAAAAAAAAAAAAAAA(){
				$datoCarrera['variable']=$this->session->userdata('usuario');
			   $this->db->select();
         	   $datoCarrera['records'] = $this->db->get('carreras');
			   $this->load->view('curso_v');
			}
			
		/*---------------------------------------------------------------------------------------*/	
		
			
			//**************INGRESSA USUARIO AL REGISTRARSE******************//
			function ingregarPersona(){				
								
				$existUsuario=$this->modelo->existeUsuario($this->input->post("usuario",true));				
				  
				if($existUsuario == '0'){
				if($this->input->post()) 
					{               
						if ($this->form_validation->run("formulario/agregar"))
						{
						   $data=array
						   (
								'nombre'=>$this->input->post("nombre",true),
								'apellidos'=>$this->input->post("apellidos",true),
								'cedula'=>$this->input->post("cedula",true),
								'correo'=>$this->input->post("correo",true),
								'usuario'=>$this->input->post("usuario",true),
								'contrasenia'=>$this->input->post("contrasenia",true),
								'categoria'=>$this->input->post("categoria",true),
								'estado'=>$this->input->post("estado",true),
								'creado_en'=>$this->input->post("creadoen",true),
								'actualizado_en'=>$this->input->post("actualizadoen",true)
								
						   );
							$guardar=$this->modelo->agregarDatos_persona($data);
							if($guardar)
							{	 
								// _____________________ENVIAR EMAIL______________________________	
								
								
								/*$this->email->from('kenneth.aguilarsalas@gmail.com','kenneth');
								$this->email->to('kenneth.aguilarsalas@gmail.com');								
								$this->email->subject('Solicitud de activación UTN');
								$this->email->message('Exito');
															
								if($this->email->send()){
									echo 'Su correo ha sido enviado';
								}
								else{								
									echo $this->email->print_debugger();
								}*/
																				
								 $this->session->set_flashdata('ControllerMessage', 'Se guardó el registro exitosamente!!!.');
						         redirect(base_url().'controlador/llamarlogin',  301);
								 
							}else
							{
								$this->session->set_flashdata('ControllerMessage', 'Se ha producido un error. Inténtelo nuevamente por favor.');
													   redirect(base_url().'registro',  301);
							}
						}					
					 
					}				    
				}
				else{
				     $this->session->set_flashdata('ControllerMessage', 'El usuario 
					                                ya existe, debe buscar otro nombre de usuario.');
												    redirect(base_url().'controlador/llamarRegistro',  301);	
					}
					$this->load->view("registro");
			}
	
	/*         **************************** FUNCIONES LOGIN Y LOGOUT  ****************************              */
			function login()
			{
				if ( $this->input->post() )
				{
					//die($this->input->post("pass",true));
					$var_login=$this->input->post("login",true);
					$var_pass=$this->input->post("pass",true);
					
					//ESTA CONSULTA DEVUELVE UN COUNT IGUAL A 0 0 1
					$datos=$this->modelo->logueo($var_login,$var_pass);					
					$datos_array=$this->modelo->estado($var_login,$var_pass);
					
					
				   //echo $datos;exit;
				   //die($datos);
				   //echo $datos_array;
				   if($datos==1)
				   {
						$this->session->set_userdata("universidad");
						//$this->session->set_userdata('login', $this->input->post('login',true));
						$this->session->set_userdata($datos_array);
						
						//$this->session->set_userdata('saludo','hola te saludo desde la sessión');
						//$session_id = $this->session->userdata('login');
						//echo $this->session->userdata('saludo');
						echo $this->session_id;
						redirect(base_url().'controlador',  301);
				   }else
				   {
					$this->session->set_flashdata('ControllerMessage', 'Usuario o clave invalida.!');
									   redirect(base_url().'controlador/login',  301);
				   }
				}
				$this->load->view("login");
			}
			
		   function logout()
			{
				$this->session->unset_userdata(array('login' => ''));
				$this->session->sess_destroy("universidad");
				redirect('controlador/login',  301);				
			}
	
	/*-----------------------------------------FIN DE FUNCIONES LOGIN Y LOGOUT----------------------------------------- */
	 /*-----------------------------------------------------------------------------------------------------------*/
	
	
	/*                             ***** CRUD DE LA TABLA PERSONAS  *****                                         */
	
	function agregarPersona()
	{		
	
	 	
		if($this->input->post()) 
            {               
                if ($this->form_validation->run("formulario/agregar"))
                {
                   $data=array
                   (
                        'nombre'=>$this->input->post("nombre",true),
                        'apellidos'=>$this->input->post("apellidos",true),
                        'cedula'=>$this->input->post("cedula",true),
						'correo'=>$this->input->post("correo",true),
                        'usuario'=>$this->input->post("usuario",true),
                        'contrasenia'=>$this->input->post("contrasenia",true),
						'categoria'=>$this->input->post("categoria",true),
                        'estado'=>$this->input->post("estado",true),
                        'creado_en'=>$this->input->post("creadoen",true),
						'actualizado_en'=>$this->input->post("actualizadoen",true)
                        
                   );					
					
                    $guardar=$this->modelo->agregarDatos_persona($data);
                    if($guardar)
                    {
                         $this->session->set_flashdata('ControllerMessage', 'Se guardó el registro exitosamente!!!.');
                					redirect(base_url().'controlador',  301);
                    }else
                    {
                        $this->session->set_flashdata('ControllerMessage', 'Se ha producido un error. Inténtelo nuevamente por favor.');
                					       redirect(base_url().'add_persona',  301);
                    }
                }
				
			 
            }
        $this->load->view("add_persona");
    
	}
	
	
	function actualizarPersona($idPersona){		
		
		$data['hecho']=0;		
				
			if($this->input->post('acedula')){
				
						
				$data_modelo=array(
						'nombre'=>$this->input->post("anombre",true),
                        'apellidos'=>$this->input->post("aapellidos",true),
                        'cedula'=>$this->input->post("acedula",true),
						'correo'=>$this->input->post("acorreo",true),
                        'usuario'=>$this->input->post("ausuario",true),
                        'contrasenia'=>$this->input->post("acontrasenia",true),
						'categoria'=>$this->input->post("acategoria",true),
                        'estado'=>$this->input->post("aestado",true),
                        'creado_en'=>$this->input->post("acreadoen",true),
						'actualizado_en'=>$this->input->post("aactualizadoen",true)					
			);					
				$this->modelo->actualizarDatos_persona($idPersona,$data_modelo);				
				$data['hecho']=1;
				$this->session->set_flashdata('ControllerMessage', 'Actualización exitosa!!!.');
				redirect(base_url().'controlador',  301);					
			} else{				
				   $data['dato']=$this->modelo->recibirDato_persona($idPersona);			
				   $this->load->view('update_persona',$data);
			}
			
	}
	
	function eliminarPersona($idPersona)
	{
		//$data['id'] = $id;
        //$this->load->view('prueba_envio',$data);
		if($this->input->post()){	
		        			
				$this->modelo->eliminarDatos_persona($idPersona);
				
				$this->session->set_flashdata('ControllerMessage', 'Se ha eliminado el registro exitosamente!!!.');
                redirect(base_url().'controlador',  301);								
			}else
             {
                $this->session->set_flashdata('ControllerMessage', 'Se ha producido un error. Inténtelo nuevamente.');
                					           redirect(base_url().'controlador',  301);
              }	
			
			//redirect('crude');
			
			//$data['dato']=$this->modelo->recibir_dato($IdEstudiante);			
			//$this->load->view('eliminar_vista',$data);
				
	}
	
	function buscar_persona() 
	{ 
		$match = $this->input->get('term', TRUE);  // TRUE para hacer un filtrado XSS  
		$item = $this->input->get('item', TRUE); 		
		
		$data['item'] = $item;
		$data['results'] = $this->modelo->buscarDato_persona($item,$match);
			
		$this->load->view('datospersona_v',$data);	
	}
	
	
	 /*-----------------------------------------FIN DE LA TABLA PERSONAS----------------------------------------- */
	 /*-----------------------------------------------------------------------------------------------------------*/	
	 
	 
	 
	 /*                            ***** CRUD DE LA TABLA GRUPOS *****                                           */
				
	function agregarGrupo()
	{		
		if($this->input->post()) 
            {                 
                   $data=array
                   (
                        'curso_id'=>$this->input->post("cursoid",true),
                        'profesor_id'=>$this->input->post("profesorid",true),
                        'numero_grupo'=>$this->input->post("numgrupo",true),
						'cuatrimestre'=>$this->input->post("cuatri",true),
                        'creado_en'=>$this->input->post("creadoen",true),
                        'actualizado_en'=>$this->input->post("actualizadoen",true)						
                        
                   );
                    $guardar=$this->modelo->agregarDatos_grupo($data);
                    if($guardar)
                    {
                         $this->session->set_flashdata('ControllerMessage', 'Se guardó el registro exitosamente!!!.');
                redirect(base_url().'controlador',  301);
                    }else
                    {
                        $this->session->set_flashdata('ControllerMessage', 'Se ha producido un error. Inténtelo nuevamente por favor.');
                					           redirect(base_url().'add_grupo',  301);
                    }
                }		 
            
        $this->load->view("add_grupo");
    
	}
	
	
	function actualizarGrupo($idGrupo)
	{		
		
		$data['hecho']=0;		
				
			if($this->input->post('aids')){
				
						
				$data_modelo=array(
				
						'curso_id'=>$this->input->post("acurid",true),
                        'profesor_id'=>$this->input->post("aprofid",true),
                        'numero_grupo'=>$this->input->post("anumgrup",true),
						'cuatrimestre'=>$this->input->post("acuat",true),
                        'creado_en'=>$this->input->post("acreaen",true),
                        'actualizado_en'=>$this->input->post("aactuaen",true)
											
			);					
				$this->modelo->actualizarDatos_grupo($idGrupo,$data_modelo);				
				$data['hecho']=1;
				$this->session->set_flashdata('ControllerMessage', 'Actualización exitosa!!!.');
				redirect(base_url().'controlador',  301);					
			} else{				
				   $data['dato']=$this->modelo->recibirDato_grupo($idGrupo);			
				   $this->load->view('update_grupo',$data);
			}
			
	}
	
	function eliminarGrupo($idGrupo)
	{
		//$data['id'] = $id;
        //$this->load->view('prueba_envio',$data);
		if($this->input->post()){	
		        			
				$this->modelo->eliminarDatos_grupo($idGrupo);
				
				$this->session->set_flashdata('ControllerMessage', 'Se ha eliminado el registro exitosamente!!!.');
                redirect(base_url().'controlador',  301);								
			}else
             {
                $this->session->set_flashdata('ControllerMessage', 'Se ha producido un error. Inténtelo nuevamente.');
                					           redirect(base_url().'controlador',  301);
              }	
			
			//redirect('crude');
			
			//$data['dato']=$this->modelo->recibir_dato($IdEstudiante);			
			//$this->load->view('eliminar_vista',$data);
				
	}
	
	function buscar_grupo() 
	{ 
		$match = $this->input->get('term', TRUE);  // TRUE para hacer un filtrado XSS  
		$item = $this->input->get('item', TRUE); 		
		
		$data['item'] = $item;
		$data['results'] = $this->modelo->buscarDato_grupo($item,$match);
			
		$this->load->view('datosgrupo_v',$data);	
	}
	
	
	
	 /*-----------------------------------------FIN DE LA TABLA GRUPOS----------------------------------------- */
	 /*-----------------------------------------------------------------------------------------------------------*/
	 
	  /*                            ***** CRUD DE LA TABLA CURSOS *****                                           */
	
	
	 /*-----------------------------------------FIN DE LA TABLA CURSOS----------------------------------------- */
	 /*-----------------------------------------------------------------------------------------------------------*/
	 
	  /*                            ***** CRUD DE LA TABLA AULAS *****                                           */
	   function agregarAula(){
					
		if($this->input->post()) 
            {                 
                   $data=array
                   (
                        'id_grupo'=>$this->input->post("idgrupo",true),
                        'nombre_aula'=>$this->input->post("nombreaula",true),
                        'ubicacion'=>$this->input->post("aubi",true),
						'codigo_aula'=>$this->input->post("codigoaula",true),
                        'finalizacion'=>$this->input->post("final",true)                        						
                        
                   );
                    $guardar=$this->modelo->agregarDatos_aula($data);
                    if($guardar)
                    {
                         $this->session->set_flashdata('ControllerMessage', 'Se guardó el registro exitosamente!!!.');
                redirect(base_url().'controlador',  301);
                    }else
                    {
                        $this->session->set_flashdata('ControllerMessage', 'Se ha producido un error. Inténtelo nuevamente por favor.');
                					           redirect(base_url().'add_grupo',  301);
                    }
                }		 
            
        $this->load->view("add_aula");
    
	}
	
	
	function actualizarAula($idAula)
	{		
		
		$data['hecho']=0;		
				
			if($this->input->post('aids')){
				
						
				$data_modelo=array(
				
					'id_grupo'=>$this->input->post("aidgru",true),
                    'nombre_aula'=>$this->input->post("anomau",true),
                    'ubicacion'=>$this->input->post("aubi",true),
				    'codigo_aula'=>$this->input->post("acodi",true),
                    'finalizacion'=>$this->input->post("afina",true)	
											
			);					
				$this->modelo->actualizarDatos_aula($idAula,$data_modelo);				
				$data['hecho']=1;
				$this->session->set_flashdata('ControllerMessage', 'Actualización exitosa!!!.');
				redirect(base_url().'controlador',  301);					
			} else{				
				   $data['dato']=$this->modelo->recibirDato_aula($idAula);			
				   $this->load->view('update_aula',$data);
			}
			
	}
	
	 function eliminarAula($idAula)
	{
		//$data['id'] = $id;
        //$this->load->view('prueba_envio',$data);
		if($this->input->post()){	
		        			
				$this->modelo->eliminarDatos_aula($idAula);
				
				$this->session->set_flashdata('ControllerMessage', 'Se ha eliminado el registro exitosamente!!!.');
                redirect(base_url().'controlador',  301);								
			}else
             {
                $this->session->set_flashdata('ControllerMessage', 'Se ha producido un error. Inténtelo nuevamente.');
                					           redirect(base_url().'controlador',  301);
              }	
				
	}
	
	function buscar_aula() 
	{ 
		$match = $this->input->get('term', TRUE);  // TRUE para hacer un filtrado XSS  
		$item = $this->input->get('item', TRUE); 		
		
		$data['item'] = $item;
		$data['results'] = $this->modelo->buscarDato_aula($item,$match);
			
		$this->load->view('datosaula_v',$data);	
	}
	
	 /*-----------------------------------------FIN DE LA TABLA AULAS----------------------------------------- */
	 /*-----------------------------------------------------------------------------------------------------------*/
	 
	  
	  
	  /*                            ***** CRUD DE LA TABLA CURSOS *****                                           */
	  function agregarCurso(){
					
		if($this->input->post()) 
            {                 
                   $data=array
                   (
                        'codigo_curso'=>$this->input->post("codigocurso",true),
                        'nombre_curso'=>$this->input->post("nombrecurso",true),
                        'descripcion'=>$this->input->post("descrip",true),
						'creado_en'=>$this->input->post("creadoen",true),
                        'actualizado_en'=>$this->input->post("actualizadoen",true)                        						
                        
                   );
                    $guardar=$this->modelo->agregarDatos_curso($data);
                    if($guardar)
                    {
                         $this->session->set_flashdata('ControllerMessage', 'Se guardó el registro exitosamente!!!.');
                redirect(base_url().'controlador',  301);
                    }else
                    {
                        $this->session->set_flashdata('ControllerMessage', 'Se ha producido un error. Inténtelo nuevamente por favor.');
                					           redirect(base_url().'add_curos',  301);
                    }
                }		 
            
        $this->load->view("add_curso");
    
	}
	
	
	function actualizarCurso($idCurso)
	{		
		
		$data['hecho']=0;		
				
			if($this->input->post('aids')){
				
						
				$data_modelo=array(
				
					    'codigo_curso'=>$this->input->post("acodigocurso",true),
                        'nombre_curso'=>$this->input->post("anombrecurso",true),
                        'descripcion'=>$this->input->post("adescrip",true),
						'creado_en'=>$this->input->post("acreadoen",true),
                        'actualizado_en'=>$this->input->post("aactualizadoen",true)	
											
			);					
				$this->modelo->actualizarDatos_curso($idCurso,$data_modelo);				
				$data['hecho']=1;
				$this->session->set_flashdata('ControllerMessage', 'Actualización exitosa!!!.');
				redirect(base_url().'controlador',  301);					
			} else{				
				   $data['dato']=$this->modelo->recibirDato_curso($idCurso);			
				   $this->load->view('update_curso',$data);
			}
			
	}
	
	function eliminarCurso($idCurso)
	{		
		if($this->input->post()){	
		        			
				$this->modelo->eliminarDatos_curso($idCurso);
				
				$this->session->set_flashdata('ControllerMessage', 'Se ha eliminado el registro exitosamente!!!.');
                redirect(base_url().'controlador',  301);								
			}else
             {
                $this->session->set_flashdata('ControllerMessage', 'Se ha producido un error. Inténtelo nuevamente.');
                					           redirect(base_url().'controlador',  301);
              }	
				
	}
	
	function buscar_curso() 
	{ 
		$match = $this->input->get('term', TRUE);  // TRUE para hacer un filtrado XSS  
		$item = $this->input->get('item', TRUE); 		
		
		$data['item'] = $item;
		$data['results'] = $this->modelo->buscarDato_curso($item,$match);
			
		$this->load->view('datoscurso_v',$data);	
	}
	
	 /*-----------------------------------------FIN DE LA TABLA CURSOS----------------------------------------- */
	 /*-----------------------------------------------------------------------------------------------------------*/
	 
	 
	 
	 
	 /*                            ***** CRUD DE LA TABLA CARRERAS *****                                           */
	 function agregarCarrera(){
					
		if($this->input->post()) 
            {                 
                   $data=array
                   (
                        'id_curso'=>$this->input->post("idcurso",true),
                        'nombre_carrera'=>$this->input->post("nombrecarrera",true),
                        'codigo_carrera'=>$this->input->post("codigocarrera",true)                      						
                        
                   );
                    $guardar=$this->modelo->agregarDatos_carrera($data);
                    if($guardar)
                    {
                         $this->session->set_flashdata('ControllerMessage', 'Se guardó el registro exitosamente!!!.');
                redirect(base_url().'controlador',  301);
                    }else
                    {
                        $this->session->set_flashdata('ControllerMessage', 'Se ha producido un error. Inténtelo nuevamente por favor.');
                					           redirect(base_url().'add_carrera',  301);
                    }
                }		 
            
        $this->load->view("add_carrera");
    
	}
	
	
	function actualizarCarrera($idCarrera)
	{		
		
		$data['hecho']=0;		
				
			if($this->input->post('aids')){
				
						
				$data_modelo=array(
				
					'id_curso'=>$this->input->post("id_curso",true),
                    'nombre_carrera'=>$this->input->post("nombre_carrera",true),
                    'codigo_carrera'=>$this->input->post("codigo_carrera",true) 
											
			);					
				$this->modelo->actualizarDatos_carrera($idCarrera,$data_modelo);				
				$data['hecho']=1;
				$this->session->set_flashdata('ControllerMessage', 'Actualización exitosa!!!.');
				redirect(base_url().'controlador',  301);					
			} else{				
				   $data['dato']=$this->modelo->recibirDato_carrera($idCarrera);			
				   $this->load->view('update_carrera',$data);
			}
			
	}
	
	function eliminarCarrera($idCarrera)
	{		
		if($this->input->post()){	
		        			
				$this->modelo->eliminarDatos_carrera($idCarrera);
				
				$this->session->set_flashdata('ControllerMessage', 'Se ha eliminado el registro exitosamente!!!.');
                redirect(base_url().'controlador',  301);								
			}else
             {
                $this->session->set_flashdata('ControllerMessage', 'Se ha producido un error. Inténtelo nuevamente.');
                					           redirect(base_url().'controlador',  301);
              }					
	}
	
	function buscar_carrera() 
	{ 
		$match = $this->input->get('term', TRUE);  // TRUE para hacer un filtrado XSS  
		$item = $this->input->get('item', TRUE); 		
		
		$data['item'] = $item;
		$data['results'] = $this->modelo->buscarDato_carrera($item,$match);
			
		$this->load->view('datoscarrera_v',$data);	
	}
	
	 /*-----------------------------------------FIN DE LA TABLA CARRERAS----------------------------------------- */
	 /*-----------------------------------------------------------------------------------------------------------*/
	 
	 /*                            ***** CRUD DE LA TABLA HORARIOS *****                                           */
	
	
	 /*-----------------------------------------FIN DE LA TABLA HORARIOS----------------------------------------- */
	 /*-----------------------------------------------------------------------------------------------------------*/
	 
	 
	 /*                            ***** CRUD DE LA TABLA REGISTROS *****                                           */
	 function agregarRegistro($idRegistro)
	{		$data['hecho']=0;		
				//printf( $this->input->post('idestu')); die();
			if($this->input->post('idestu')){			
						
				$data_modelo=array(
				
					'id_estidiante'=>$this->input->post("aidestu",true),
                    'id_grupo'=>$this->input->post("aidgrupo",true)                    
											
			);					
				$this->modelo->agregarDatos_registro($data_modelo);				
				$data['hecho']=1;
				$this->session->set_flashdata('ControllerMessage', 'Actualización exitosa!!!.');
				redirect(base_url().'controlador',  301);					
			} else{				
				   $data['dato']=$this->modelo->recibirDato_registro($idRegistro);
				   
         	       $data['records'] = $this->db->query("SELECT * FROM grupos");			
				   $this->load->view('add_matricula',$data);
			}
		}
	
	
	
	 /*-----------------------------------------FIN DE LA TABLA REGISTROS----------------------------------------- */
	 /*-----------------------------------------------------------------------------------------------------------*/
}
?>