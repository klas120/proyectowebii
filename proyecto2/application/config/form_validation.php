<?php
$config=array
(    
	'formulario/agregar'
		=> array(			
            array('field' => 'nombre',	'label' => 'Nombre', 'rules' => 'required|is_string|trim|xss_clean'),
             array('field' => 'apellidos', 'label' => 'Apellidos', 'rules' => 'required|is_string|trim|xss_clean'),
             array('field' => 'cedula',	'label' => 'Cedula', 'rules' => 'required|numeric|trim|xss_clean'),
			  array('field' => 'correo',	'label' => 'Correo', 'rules' => 'required|valid_email|trim|xss_clean'),			  
			  array('field' => 'usuario',	'label' => 'Usuario', 'rules' => 'required|is_string|trim|xss_clean'),
			  array('field' => 'contrasenia',	'label' => 'Contraseña', 'rules' => 'required|is_string|trim|xss_clean')		  
			  
		),        
); 