<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

  class Modelo extends CI_Model {

	function __construct(){
		parent::__construct();
	}
		
	   //BUSCA (ESTUDIANTES, PROFESORES Y ADMINISTRATIVOS)
	   function tablaPersona($idPersona){
			$this->db
			->select('cedula,nombre,apellidos,correo')
			->from('personas')
			->where("id",$idPersona);
			$query=$this->db->get();	
			//echo $this->db->last_query();
			return $query;
	   }
	   //BUSCA LA RELACION ESTUDIANTE Y GRUPO
	   function tablaRegistro($idRegistro){
			$this->db
			->select()->from('registros')->where("id",$idRegistro);
			$query=$this->db->get();	
			//echo $this->db->last_query();
			return $query;
	   }   
	
	
	/*     *********************  LOGUEO DE PERSONAS  ************************                        */
	
	function logueo($login,$pass)
	   {
			 $query=$this->db
			->select()
			->from("personas")
			->where(array("usuario"=>$login,"contrasenia"=>$pass))
			->count_all_results();
			//echo $this->db->last_query();
			return $query;
	   }
	   
	   function estado($login,$pass)
	   {
			$this->db
			->select()
			->from("personas")
			->where(array("usuario"=>$login,"contrasenia"=>$pass));
			$query=$this->db->get();	
			//echo $this->db->last_query();
			return $query->first_row('array');			
			
	   }
		//BUSCAR EXISTENCIA DE UN USUARIO
		function existeUsuario($usuario)
	   {
			$query=$this->db
			->select()
			->from("personas")
			->where(array("usuario"=>$usuario))
			->count_all_results();
			//echo $this->db->last_query();
			return $query;		
			
	   }
	
	/*-----------------------------------------FIN DEL LOG------------------------------------------------------ */
	/*---------------------------------------------------------------------------------------------------------- */
	
	
	/*                             ***** CRUD DE LA TABLA PERSONAS  *****                                         */
	function buscarDato_persona($item,$match) {      
		 $this->db->like($item, $match); 	 
		 $query = $this->db->get('personas'); 	 
		 return $query->result();	      
	}
	
	function recibirDato_persona($id){
			$data['vacio']=0;
			$query=$this->db->select()
			         ->from('personas')
					 ->where(array('id'=>$id))
					 ->order_by('id','desc');
			
			$query=$this->db->get();
			if($query->num_rows()>0){
				return $query->first_row('array');
			} else{
				return false;
			  }
		}	
	
    function actualizarDatos_persona($id,$data){
			$this->db->where('id',$id);
			$this->db->update('personas',$data);
		}	
	
	function agregarDatos_persona($datos=array()){
        $this->db->insert("personas",$datos);
        return true;
    }
	
	function eliminarDatos_persona($Id){
			$this->db->where('id',$Id);
			$this->db->delete('personas');
		}
	
	 /*-----------------------------------------FIN DE LA TABLA PERSONAS----------------------------------------- */	 
	 
	 
	 
	 
	 /*                            ***** CRUD DE LA TABLA GRUPOS *****                                           */
	     
		 
		 //BUSCA DATOS PARA MOSTRARLOS EN DATALIST
        function buscarDato_grupo($item,$match) {      
			 $this->db->like($item, $match); 	 
			 $query = $this->db->get('grupos'); 	 
			 return $query->result();	      
		}
		//RETORNA UN ARREGLO PARA MOSTRARLO EN EL RESULTADO DE BUSQUEDA DEL DATALIST
		function recibirDato_grupo($id){
				$data['vacio']=0;
				$query=$this->db->select()
						 ->from('grupos')
						 ->where(array('id'=>$id))
						 ->order_by('id','desc');
				
				$query=$this->db->get();
				if($query->num_rows()>0){
					return $query->first_row('array');
				} else{
					return false;
				  }
			}	
		
		function actualizarDatos_grupo($id,$data){
				$this->db->where('id',$id);
				$this->db->update('grupos',$data);
			}	
		
		function agregarDatos_grupo($datos=array()){
			$this->db->insert("grupos",$datos);
			return true;
		}
		
		function eliminarDatos_grupo($Id){
				$this->db->where('id',$Id);
				$this->db->delete('grupos');
			}
	
	 /*-----------------------------------------FIN DE LA TABLA GRUPOS----------------------------------------- */
	 
	 
	  /*                            ***** CRUD DE LA TABLA CURSOS *****                                           */
	
	
	 /*-----------------------------------------FIN DE LA TABLA CURSOS----------------------------------------- */
	 
	 	
	 
	  /*                            ***** CRUD DE LA TABLA AULAS *****                                           */
		function buscarDato_aula($item,$match) {      
			 $this->db->like($item, $match); 	 
			 $query = $this->db->get('aulas'); 	 
			 return $query->result();	      
		}
		
		function recibirDato_aula($id){
				$data['vacio']=0;
				$query=$this->db->select()
						 ->from('aulas')
						 ->where(array('id'=>$id))
						 ->order_by('id','desc');
				
				$query=$this->db->get();
				if($query->num_rows()>0){
					return $query->first_row('array');
				} else{
					return false;
				  }
			}	
		
		function actualizarDatos_aula($id,$data){
				$this->db->where('id',$id);
				$this->db->update('aulas',$data);
			}	
		
		function agregarDatos_aula($datos=array()){
			$this->db->insert("aulas",$datos);
			return true;
		}
		
		function eliminarDatos_aula($Id){
				$this->db->where('id',$Id);
				$this->db->delete('aulas');
			}
	  
	 /*-----------------------------------------FIN DE LA TABLA AULAS----------------------------------------- */
	 
	  
	  
	  /*                            ***** CRUD DE LA TABLA CURSOS *****                                           */	
			
		function buscarDato_curso($item,$match) {      
			 $this->db->like($item, $match); 	 
			 $query = $this->db->get('cursos'); 	 
			 return $query->result();	      
		}
		
		function recibirDato_curso($id){
				$data['vacio']=0;
				$query=$this->db->select()
						 ->from('cursos')
						 ->where(array('id'=>$id))
						 ->order_by('id','desc');
				
				$query=$this->db->get();
				if($query->num_rows()>0){
					return $query->first_row('array');
				} else{
					return false;
				  }
			}	
		
		function actualizarDatos_curso($id,$data){
				$this->db->where('id',$id);
				$this->db->update('cursos',$data);
			}	
		
		function agregarDatos_curso($datos=array()){
			$this->db->insert("cursos",$datos);
			return true;
		}
		
		function eliminarDatos_curso($Id){
				$this->db->where('id',$Id);
				$this->db->delete('cursos');
			}
	
	 /*-----------------------------------------FIN DE LA TABLA CURSOS----------------------------------------- */
	  
	  
	  /*                            ***** CRUD DE LA TABLA CARRERAS *****                                           */
	  	
	function buscarDato_carrera($item,$match) {      
		 $this->db->like($item, $match); 	 
		 $query = $this->db->get('carreras'); 	 
		 return $query->result();	      
	}
	
	function recibirDato_carrera($id){
			$data['vacio']=0;
			$query=$this->db->select()
			         ->from('carreras')
					 ->where(array('id'=>$id))
					 ->order_by('id','desc');
			
			$query=$this->db->get();
			if($query->num_rows()>0){
				return $query->first_row('array');
			} else{
				return false;
			  }
		}	
	
    function actualizarDatos_carrera($id,$data){
			$this->db->where('id',$id);
			$this->db->update('carreras',$data);
		}	
	
	function agregarDatos_carrera($datos=array()){
        $this->db->insert("carreras",$datos);
        return true;
    }
	
	function eliminarDatos_carrera($Id){
			$this->db->where('id',$Id);
			$this->db->delete('carreras');
		}
	
	 /*-----------------------------------------FIN DE LA TABLA CARRERAS----------------------------------------- */
	 
	 /*                            ***** CRUD DE LA TABLA HORARIOS *****                                           */
	
	
	 /*-----------------------------------------FIN DE LA TABLA HORARIOS----------------------------------------- */
	 
	 /*                            ***** CRUD DE LA TABLA REGISTROS *****                                           */
	 	
    	
	function agregarDatos_registro($datos=array()){
        $this->db->insert("registros",$datos);		
        return true;
    }
	
	
	
	function recibirDato_registro($id){
			$data['vacio']=0;
			$query=$this->db->select()
			         ->from('registros')
					 ->where(array('id'=>$id))
					 ->order_by('id','desc');
			
			$query=$this->db->get();
			if($query->num_rows()>0){
				return $query->first_row('array');
			} else{
				return false;
			  }
		}	
	
	 /*-----------------------------------------FIN DE LA TABLA REGISTROS----------------------------------------- */
	
	
}
?>