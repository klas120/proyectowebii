<!doctype html>
<html lang="en"><head>
	<meta charset="utf-8">
	<title>Universidad Técnica Nacional</title>
	
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://localhost/proyecto2/dist/css/bootstrap.min.css">
    
    <!-- Optional theme -->
    <link rel="stylesheet" href="http://localhost/proyecto2/dist/css/bootstrap-theme.min.css">
    
    <!-- Latest compiled and minified JavaScript -->
    <script src="http://localhost/proyecto2/js/bootstrap.min.js"></script>
	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
	<link href="http://localhost/proyecto2/SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css">
	<script src="http://localhost/proyecto2/SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
	
    <link rel="stylesheet" type="text/css" href="http://localhost/proyecto2/js/jquery-ui.css" />
	<script src="http://localhost/proyecto2/js/jquery-1.9.1.js" type="text/javascript"></script>	
	<script type="text/javascript" src="http://localhost/proyecto2/js/jquery-ui.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){
	
		 $('tr:odd').css('background', '#e3e3e3');
		 var url = 'controlador/buscar_persona'; 
	 	
	 $('#cedula').autocomplete({
		 
		source: url+'?item=cedula',
		
		
		select: function(event, ui){		
			
			
			$('#resultado').slideUp('slow',function(){										
				
				$('#resultado').html(
				
				'<br>'+
				'<div id="resultado" class="alert alert-info tamanio" >'+        	
         
				'<h2 align="center"><ins>Detalles del usuario.</ins></h2>' +				 
				'<strong>Nombre: </strong>' + ui.item.nombre + ' ' + ui.item.apellidos + '<br>'+ 
				'<strong>Cedula: </strong>' + ui.item.cedula + '<br>'+				
				'<strong>Correo: </strong>' + ui.item.correo + '<br>'+
				'<strong>Categoria: </strong>' + ui.item.categoria + '<br>'+
				'<strong>Usuario: </strong>' + ui.item.usuario + '<br>'+
				'<strong>Contraseña: </strong>' + ui.item.contrasenia + '<br>'+
				'<strong>Estado: </strong>' + ui.item.estado + '<br>'+
				'<strong>Fecha Creación: </strong>' + ui.item.creado_en + '<br>'+
				'<strong>Fecha Actualización: </strong>' + ui.item.actualizado_en + '<br>'+				
				'<strong>ID:</strong>' + ui.item.id +								
				
				'<hr>'+	
				
				'<div align="center">' +
					'<div>'+
					'<form name="form1" action="controlador/eliminarPersona/'+ ui.item.id +'" method="post" >'+						
						'<input class="btn btn-danger" type="submit" name="elimi" value="Eliminar" />'+
					'</form>' + ' '+
					
					'<form name="form2" action="controlador/actualizarPersona/'+ ui.item.id +'" method="post" >'+
						'<input type="hidden" name="ids"  value="'+ ui.item.id +'" />' +
						'<input type="hidden" name="ced"  value="'+ ui.item.cedula +'" />' +
						'<input type="hidden" name="nom"  value="'+ ui.item.nombre +'" />' +
						'<input type="hidden" name="ap"  value="'+ ui.item.apellidos +'" />' +
						'<input type="hidden" name="corr"  value="'+ ui.item.correo +'" />' +
						'<input type="hidden" name="cat"  value="'+ ui.item.categoria +'" />' +
						'<input type="hidden" name="usu"  value="'+ ui.item.usuario +'" />' +
						'<input type="hidden" name="contra"  value="'+ ui.item.contrasenia +'" />' +
						'<input type="hidden" name="esta"  value="'+ ui.item.estado +'" />' +
						'<input type="hidden" name="creaen"  value="'+ ui.item.creado_en +'" />' +
						'<input type="hidden" name="actualien"  value="'+ ui.item.actualizado_en +'" />' +
												
						'<input class="btn btn-primary" type="submit" name="edi"  value="Editar" />' +
					'</form>' +
					'</div>'+
				
				'</div>'
				
		       );
			});
			$('#resultado').slideDown('slow');
			
		 }	
	  });		
	
	});	
	
</script>
</head>
<body>

<div id="container">
	
  <ul id="MenuBar1" class="MenuBarHorizontal">
    <li><a href="#">Principal</a></li>
    <li><a class="MenuBarItemSubmenu" href="#">Cursos</a>
      <ul>
        <li><a href="#">Agregar carrera a curso</a></li>        
      </ul>
    </li>
    <li><a href="#">Grupos</a>
      <ul>
        <li><a href="#">Agregar curso a grupo</a></li> 
        <hr>
        <li><a href="#">Agregar profesor a grupo</a></li>
        <hr>
        <li><a href="#">Agregar horario a grupo</a></li>
        <hr> 
        <li><a href="#">Agregar aula a grupo</a></li>      
      </ul>
    </li>
    <li><a class="#" href="#">Matricular</a>
       <ul>
        <li><a href="#">Agregar estudiante a grupo</a></li>       
      </ul>
    </li>
    <li><a href="#">Personas</a></li>
    <li><a href="#">Activar</a></li>
    <li><a href="<?php echo base_url()?>controlador/logout">Cerrar seción</a></li>
    <li><p></p></li>
    <li><p></p></li>
    <li><p> Bienvenid@: <?php echo $variable;?></p></li>
  </ul>
  <h1></h1>

	<div id="body">	<br><br>
    	
        <h1 align="center">
        
        <!--MANEJO DE ERRORES EN EL CRUD-->
		<?php 
			if ( $this->session->flashdata('ControllerMessage') != '' ) 
				{
			?>
			<div class="alert alert-success"><?php echo $this->session->flashdata('ControllerMessage'); ?></div>
			<?php 
			} 
		?>   
      </h1>
      
	   <div id="body">
            <!--ME LLEVA A LA PÁGINA AGREGAR PERSONA-->
            <a href="<?php echo base_url()?>controlador/agregarPersona">Agregar</a><br>           
           
            <label>Buscar:</label>	
            <input type="text" id="cedula" size="15" />
            
          <!--DIV DONDE SE MUESTRA EL RESULTADO DE LA BUSQUEDA-->  
        <div id="resultado">
        	
         </div>
         
         <br><br><br><br>
        	<!--CREA LA TABLA CON LOS DATOS-->
        	<?php echo $this->table->generate($records); ?>     
	    </div>
		</div>

	<p class="footer">
    
    
    </p>
</div>
<script type="text/javascript">
var MenuBar1 = new Spry.Widget.MenuBar("MenuBar1", {imgDown:"http://localhost/proyecto2/SpryAssets/SpryMenuBarDownHover.gif", 
                                                    imgRight:"http://localhost/proyecto2/SpryAssets/SpryMenuBarRightHover.gif"});
</script>
</body></html>