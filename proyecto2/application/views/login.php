<!doctype html>
<html lang="en"><head>
	<meta charset="utf-8">
	<title>Universidad Técnica Nacional</title>
	
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://localhost/proyecto2/dist/css/bootstrap.min.css">
    
    <!-- Optional theme -->
    <link rel="stylesheet" href="http://localhost/proyecto2/dist/css/bootstrap-theme.min.css">
    
    <!-- Latest compiled and minified JavaScript -->
    <script src="http://localhost/proyecto2/js/bootstrap.min.js"></script>	
	<link href="http://localhost/proyecto2/SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css">
	<script src="http://localhost/proyecto2/SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
	
    <link rel="stylesheet" type="text/css" href="http://localhost/proyecto2/js/jquery-ui.css" />
	<script src="http://localhost/proyecto2/js/jquery-1.9.1.js" type="text/javascript"></script>	
	<script type="text/javascript" src="http://localhost/proyecto2/js/jquery-ui.js"></script>
	
    
    </head>
    <body>
    <br><br><br>
    <div align="center" class="jumbotron">
    <div align="left"><a href="<?php echo base_url()?>controlador/llamarRegistro">( Registrarse )</a></div>
    
    
    
		<?php 
        if ( $this->session->flashdata('ControllerMessage') != '' ) 
            {
        ?>
        <p style="color: red;"><?php echo $this->session->flashdata('ControllerMessage'); ?></p>
        <?php 
        } 
        ?>
        
        <?php
        $atributos = array( 'id' => 'form','name'=>'form');
        echo form_open(null,$atributos);
        ?>
        <?php echo validation_errors(); ?>
        <p>
        Login: <input type="text" name="login" value="<?php echo set_value("login")?>" />
        </p>
        <p>
        Password: <input type="password" name="pass" value="<?php echo set_value("pass")?>" />
        </p>
        
        <hr />
        <input class="btn btn-primary" type="submit" value="Enviar" title="Enviar" />
        <?php
        echo form_close();
        ?>
	
</div>
</body>
</html>