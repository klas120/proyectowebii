<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title></title>
<!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://localhost/proyecto2/dist/css/bootstrap.min.css">
    
    <!-- Optional theme -->
    <link rel="stylesheet" href="http://localhost/proyecto2/dist/css/bootstrap-theme.min.css">
    
    <!-- Latest compiled and minified JavaScript -->
    <script src="http://localhost/proyecto2/js/bootstrap.min.js"></script>
	<style type="text/css">

	::selection{ background-color: #E13300; color: white; }
	::moz-selection{ background-color: #E13300; color: white; }
	::webkit-selection{ background-color: #E13300; color: white; }

	body {
		background-color: #fff;
		margin: 40px;
		font: 13px/20px normal Helvetica, Arial, sans-serif;
		color: #4F5155;
	}

	a {
		color: #003399;
		background-color: transparent;
		font-weight: normal;
	}

	h1 {
		color: #444;
		background-color: transparent;
		border-bottom: 1px solid #D0D0D0;
		font-size: 19px;
		font-weight: normal;
		margin: 0 0 14px 0;
		padding: 14px 15px 10px 15px;
	}

	code {
		font-family: Consolas, Monaco, Courier New, Courier, monospace;
		font-size: 12px;
		background-color: #f9f9f9;
		border: 1px solid #D0D0D0;
		color: #002166;
		display: block;
		margin: 14px 0 14px 0;
		padding: 12px 10px 12px 10px;
	}

	#body{
		margin: 0 15px 0 15px;
	}
	
	p.footer{
		text-align: right;
		font-size: 11px;
		border-top: 1px solid #D0D0D0;
		line-height: 32px;
		padding: 0 10px 0 10px;
		margin: 20px 0 0 0;
	}
	
	#container{
		margin: 10px;
		border: 1px solid #D0D0D0;
		-webkit-box-shadow: 0 0 8px #D0D0D0;
	}
	</style>
	<link href="http://localhost/proyecto2/SpryAssets/SpryMenuBarHorizontal.css" rel="stylesheet" type="text/css">
	<script src="http://localhost/proyecto2/SpryAssets/SpryMenuBar.js" type="text/javascript"></script>
	
    <link rel="stylesheet" type="text/css" href="http://localhost/proyecto2/js/jquery-ui.css" />
	<script src="http://localhost/proyecto2/js/jquery-1.9.1.js" type="text/javascript"></script>	
	<script type="text/javascript" src="http://localhost/proyecto2/js/jquery-ui.js"></script>

</head>

<body>
<h1>Actualizar persona</h1>
<p>
<a href="<?php echo base_url()?>controlador">Regresar</a>
</p>

	<?php
$atributos = array( 'id' => 'form','name'=>'form');
//echo form_open(null, $atributos);
echo form_open_multipart(null,$atributos);
 
?>

<p>
Nombre: <input type="text" name="anombre" value="<?php echo $this->input->post("nom")?>" />
</p>
<p>
Apellidos: <input type="text" name="aapellidos" value="<?php echo $this->input->post("ap")?>" />
</p>
<p>
Cedula: <input  type="text" name="acedula" value="<?php echo $this->input->post("ced")?>" />
</p>

<p>
Correo: <input type="text" name="acorreo" value="<?php echo $this->input->post("corr")?>" />
</p>
<p>
Usuario: <input type="text" name="ausuario" value="<?php echo $this->input->post("usu")?>" />
</p>
<p>
Contraseña: <input type="text" name="acontrasenia" value="<?php echo $this->input->post("contra")?>" />
</p>
      
<p>
Categoría:
<select name="acategoria">
 <option selected><?php echo $this->input->post('cat'); ?></option>
  <option value="estudiante">Estudiante</option>
  <option value="profesor">Profesor</option>
  <option value="administrativo">Administrativo</option>  
</select>
</p>

<p>
Categoría:
<select name="aestado">
 <option selected><?php echo $this->input->post('esta'); ?></option>
  <option value="inactivo">Inactivo</option>
  <option value="activo">Activo</option>   
</select>
</p>

<p>
Fecha creado: <input type="text" name="acreadoen" value="<?php echo $this->input->post('creaen'); ?>" />
</p>

<p>
Fecha Actualizado: <input type="text" name="aactualizadoen" value="<?php echo $this->input->post('actualien'); ?>" />
</p>


<hr />
<input type="submit" value="Enviar" title="Enviar" />


<?php
echo form_close();
?>
</body>
</html>